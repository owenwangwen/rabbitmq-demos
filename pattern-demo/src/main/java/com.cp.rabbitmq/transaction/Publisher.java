package com.cp.rabbitmq.transaction;


import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.cp.rabbitmq.utils.ConnectionUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
/**
 * @author Coder编程
 * @version V1.0
 * @Title: Publisher
 * @Package: com.cp.rabbitmq.transaction
 * @Description: AMQP事务机制
 * @date 2019/8/7  19:54
 **/

public class Publisher {


    private static final String QUEUE_NAME="TEST_QUEUE_TX";

    public static void main(String[] args) throws IOException, TimeoutException {

        Connection connection = ConnectionUtils.getConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);
        String msgString="hello transaction message!";

        try {
            channel.txSelect();
            channel.basicPublish("", QUEUE_NAME, null,msgString.getBytes());
            int xx=1/0;
            System.out.println("send "+msgString);
            channel.txCommit();
        } catch (Exception e) {
            channel.txRollback();
            System.out.println(" send message transactionRollback");
        }

        channel.close();
        connection.close();

    }
}
