package com.cp.rabbitmq.publishersubscribe;


import com.cp.rabbitmq.utils.ConnectionUtils;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author Coder编程
 * @version V1.0
 * @Title: ConsumerOne
 * @Package: com.cp.rabbitmq.publishersubscribe
 * @Description: 发布订阅模式
 * @date 2019/7/10  19:54
 **/

public class ConsumerOne {

    private static final String QUEUE_NAME="TEST_QUEUE_EMAIL_FANOUT";

    private static final String  EXCHANGE_NAME="TEST_EXCHANGE_FANOUT";

    public static void main(String[] args) throws IOException, TimeoutException {
        Connection connection = ConnectionUtils.getConnection();
        final Channel channel = connection.createChannel();

        //队列声明
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        //绑定队列到交换机 转发器
        channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "");


        channel.basicQos(1);//保证一次只分发一个

        //定义一个消费者
        Consumer consumer=new DefaultConsumer(channel){
            //消息到达 触发这个方法
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {

                String msg=new String(body,"utf-8");
                System.out.println("[ConsumerOne] Received msg:"+msg);

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally{
                    System.out.println("[ConsumerOne] done ");
                    channel.basicAck(envelope.getDeliveryTag(), false);
                }
            }
        };

        boolean autoAck=false;//自动应答 false
        channel.basicConsume(QUEUE_NAME,autoAck , consumer);
    }
}
